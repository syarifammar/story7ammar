from django.test import TestCase, Client
from django.urls import resolve
import time
from .views import index

class UnitTestStory7(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    def test_function_story7(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    def test_index_is_using_correct_html(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home/index.html')
    def test_landing_page_is_written(self):
        self.assertIsNotNone(index)

